**Before opening a new feature request**
* [ ] Search [existing issues](https://gitlab.com/gparent/f1-2022-telemetry/-/issues?scope=all) for keywords related to the feature
* [ ] Read the [latest documentation](https://f1-2022-telemetry.readthedocs.io/en/latest/) to make sure it doesn't exist already
* [ ] Test with the master branch if possible

**Feature description**
<!-- What the feature is. Be precise. -->

**Use cases**
<!-- How would users of the library use this feature? Is it useful to others? -->

**Example code**
<!-- If you have an idea of how the feature would be used, insert an example -->
