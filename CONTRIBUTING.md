# How to contribute

Thank you for considering to contribute to this project!

These guidelines will help get your contributions accepted into the codebase

## Getting Started

* Make sure you have a [Gitlab account](https://gitlab.com/) and fork the project
* Create a topic branch from master (`git checkout -b feature/my_feature master`)
* Make commits of logical and atomic units
* Commits must be checked with pre-commit and all issues fixed
* Push your changes to a topic branch in your fork of the repository
* Send a [merge request](https://gitlab.com/gparent/f1-2022-telemetry/-/merge_requests/new?merge_request[target_branch]=master&merge_request[target_project_id]=19733853) to the [upstream](https://gitlab.com/gparent/f1-2020-telemetry) repository

## Additional Resources

* Gitlab's [merge request documentation](https://docs.gitlab.com/ee/user/project/merge_requests/)
